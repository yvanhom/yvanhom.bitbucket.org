local md = require "markdown.markdown"

local function readfile(file)
    local f = io.open(file, "r")
    local t = f:read("*all")
    f:close()
    return t
end

local function writefile(file, text)
    local f = io.open(file, "w")
    f:write(text)
    f:close()
end

local function loadTemplate(dir)
    local t = {}
    t.header = readfile(dir .. "/header.txt")
    t.footer = readfile(dir .. "/footer.txt")
    t.page = readfile(dir .. "/page.txt")
    t.sidebar = dofile(dir .. "/sitebar.lua")
    
    t.all = t.header ..  t.page .. t.footer
    return t
end

local function loadConfig(dir)
    local c = dofile(dir .. "/config.lua")
    
    for k, v in pairs(c) do
        v.sitebarfile = dir .. "/" .. v.sitebarname .. ".lua"
    end
    
    return c
end

local function updateSitebar(configs, template)
    for name, conf in pairs(configs) do
        local areas = dofile(conf.sitebarfile)
        local text = ""
        for i, area in pairs(areas) do
            text = text .. template.sidebar(area)
        end
        conf.sitebar = text
    end
end

local function loadPageList(dir)
    local pl = dofile(dir .. "/list.lua")
    
    for i, v in pairs(pl) do
        v.contentfile = dir .. "/" .. v.name .. ".md"
    end
    
    return pl
end

local function generatePage(page, config, tmp)
    local mdtext = readfile(page.contentfile)
    local content = md(mdtext)
    
    local reptable = {
        ["pagetitle"] = page.title,
        ["pagedate"] = page.date,
        ["pagecontent"] = content,

        ["siteowner"] = config.siteowner,
        ["ownermail"] = config.ownermail,
        ["sitetitle"] = config.sitetitle,
        ["siteyear"] = config.siteyear,
        ["sitebar"] = config.sitebar,
    }
    
    return string.gsub(tmp, "{{(%w+)}}", reptable)
end

local function generate(configs, pagelist, template, todir)

    for i, v in ipairs(pagelist) do
        local destfile = todir .. "/" .. v.name .. ".html"
        print("Geneate " .. destfile)
        local text = generatePage(v, configs[v.config], template.all)
        
        writefile(destfile, text)
    end
    
end

local function run(fromdir, templatedir, todir)
    local template = loadTemplate(templatedir)
    local configs = loadConfig(fromdir)
    updateSitebar(configs, template)
    local pagelist = loadPageList(fromdir)
    generate(configs, pagelist, template, todir)
end

if #arg ~= 3 then
    print("lua yvgensite.lua sourceDir templateDir targetDir")
    os.exit()
end

run(arg[1], arg[2], arg[3])
