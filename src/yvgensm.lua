local header = [[<?xml version="1.0"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
]]
local footer = [[
</urlset>
]]

local pagetemp = [[
<url>
    <loc>{{site}}/{{name}}.html</loc>
    <lastmod>{{date}}T00:00:00+00:00</lastmod>
</url>
]]

local function genSiteMap(sitedir, url, destdir)
    local destfile = destdir .. "/sitemap.xml"
    
    local outfile = io.open(destfile, "w")
    outfile:write(header)
    
    local reptable = {
        site = url,
        name = nil,
        date = nil,
    }
    local listfile = sitedir .. "/list.lua"
    local pl = dofile(listfile)
    local pageitem
    for i, page in pairs(pl) do
        reptable.name = page.name
        reptable.date = page.date
        pageitem = string.gsub(pagetemp, "{{(%w+)}}", reptable)
        outfile:write(pageitem)
    end
    
    outfile:write(footer)
    outfile:close()
end

if #arg ~= 3 then
    print("Usage: sitedir url destdir")
    os.exit()
end

genSiteMap(arg[1], arg[2], arg[3])
