### 信息

**游戏主页**: [itch.io](https://nomnomnami.itch.io/her-tears-were-my-light)

**游戏作者**: [Nami](https://nomnomnami.itch.io/)

**游戏类型**: 视觉小说

**平均时长**: 大约半个小时

**游戏描述**:

“她的眼泪曾是我的光芒”是一部视觉小说，参与 NaNoRenO 2016，讲述着一个 Time 和 Space 间的简短爱情故事，您可以向前跳跃时间，回退时间，重回任意存档的时间点，那么您是否能用这股力量拯救 Space 的孤独？

### 翻译

**中文名**: 她的眼泪曾是我的光芒

**共享协议**: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

**Git库**: [bitbucket](https://bitbucket.org/yvanhom/hertearsweremylight-cn/)

**下载**: [bitbucket downloads](https://bitbucket.org/yvanhom/hertearsweremylight-cn/downloads)

**操作指南**:

1. 从 [itch.io](https://nomnomnami.itch.io/her-tears-were-my-light) 下载游戏，并解压压缩包。
2. 从 [bitbucket downloads](https://bitbucket.org/yvanhom/hertearsweremylight-cn/downloads) 下载中文补丁文件，文件名为 HTWML-cnpatch-VERSION.zip，解压补丁压缩包。
3. 将补丁文件夹跟游戏文件夹合并，注意保持两个的 game 目录合并，但没有文件的覆盖。
