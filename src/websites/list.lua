return {
    {
        name = "index",
        title = "Yuri Games Yvan Hom Translates",
        date = "2016-07-21",
        config = "english"
    },
    {
        name = "index-cn",
        title = "Yvan Hom 所翻译的百合作品",
        date = "2016-07-21",
        config = "chinese"
    },
    {
        name = "her-tears-were-my-light",
        title = "Her Tears Were My Light",
        date = "2016-07-28",
        config = "english"
    },
    {
        name = "her-tears-were-my-light-cn",
        title = "Her Tears Were My Light",
        date = "2016-07-28",
        config = "chinese"
    },
    {
        name = "the-sad-story-of-emmeline-burns",
        title = "The Sad Story of Emmeline Burns",
        date = "2016-07-28",
        config = "english"
    },
    {
        name = "the-sad-story-of-emmeline-burns-cn",
        title = "The Sad Story of Emmeline Burns",
        date = "2016-07-28",
        config = "chinese"
    },
    {
        name = "romance-detective",
        title = "Romance Detective",
        date = "2016-07-28",
        config = "english"
    },
    {
        name = "romance-detective-cn",
        title = "Romance Detective",
        date = "2016-07-28",
        config = "chinese"
    },
    {
        name = "princess-maid",
        title = "Princesses's Maid",
        date = "2016-08-21",
        config = "english"
    },
    {
        name = "princess-maid-cn",
        title = "Princesses's Maid",
        date = "2016-08-21",
        config = "chinese"
    },
    {
        name = "written-in-the-sky",
        title = "Written in the Sky",
        date = "2016-09-10",
        config = "english"
    },
    {
        name = "written-in-the-sky-cn",
        title = "Written in the Sky",
        date = "2016-09-10",
        config = "chinese"
    },
}
