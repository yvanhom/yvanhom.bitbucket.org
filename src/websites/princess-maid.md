### Information

**Home Page**: [itch.io](https://toki-production.itch.io/princessess-maid)

**Author**: [Toki Production](https://toki-production.itch.io/)

**Type**: Visual Novel

**Average duration**: About a half-hour

**Descriptions**:

The twin princesses are going to get married to people they don't even know in 13 days for the sake of their kingdom. What can their childhood friend, a mere powerless maid do? 

### My translations

**LICENSE**: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

**Git repo**: [bitbucket](https://bitbucket.org/yvanhom/princessmaid-cn/)

**Download**: [bitbucket downloads](https://bitbucket.org/yvanhom/princessmaid-cn/downloads)

**Instructions**:

1. Download the game from [itch.io](https://toki-production.itch.io/princessess-maid), and extract the tarball.
2. Download the translation patch (named PrincessMaid-cnpatch-VERSION.zip) from [bitbucket downloads](https://bitbucket.org/yvanhom/princessmaid-cn/downloads), extract the tarball.
3. Merge the game directory and the path directory, by keeping the "game" directory in the same level. However, no files overwriting.



