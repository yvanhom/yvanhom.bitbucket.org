Translating some yuri games in English to Chinese voluntarily is one of my hobbies.

Accomplished:

* [Her Tears Were My Light](her-tears-were-my-light.html)
* [The Sad Story of Emmeline Burns](the-sad-story-of-emmeline-burns.html)
* [Romance Detective](romance-detective.html)
* [Princesses's Maid](princess-maid.html)
* [Written in the Sky](written-in-the-sky.html)

In progress:

* A Little Lily Princess

