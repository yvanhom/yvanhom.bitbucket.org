### Information

**Home Page**: [itch.io](https://nomnomnami.itch.io/her-tears-were-my-light)

**Author**: [Nami](https://nomnomnami.itch.io/)

**Type**: Visual Novel

**Average duration**: About a half-hour

**Descriptions**:

HER TEARS WERE MY LIGHT is a visual novel created for NaNoRenO 2016. it's a short love story about time and space. as time, you can move forward, backward, or warp to any moment you create a save point for. can you use that power to save space from her feelings of loneliness?

### My translations

**LICENSE**: [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)

**Git repo**: [bitbucket](https://bitbucket.org/yvanhom/hertearsweremylight-cn/)

**Download**: [bitbucket downloads](https://bitbucket.org/yvanhom/hertearsweremylight-cn/downloads)

**Instructions**:

1. Download the game from [itch.io](https://nomnomnami.itch.io/her-tears-were-my-light), and extract the tarball.
2. Download the translation patch (named HTWML-cnpatch-VERSION.zip) from [bitbucket downloads](https://bitbucket.org/yvanhom/hertearsweremylight-cn/downloads), extract the tarball.
3. Merge the game directory and the path directory, by keeping the "game" directory in the same level. However, no files overwriting.



