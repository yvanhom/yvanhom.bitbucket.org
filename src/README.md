### About websites/config.lua

Add different configurations for the websites. 

```lua
return {
    english = {
        siteowner = "Yvan Hom",
        sitetitle = "The Works Yvan Hom Translates",
        siteyear = "2016",
        
        -- sitebarname refers to the file websites/${sitebarname}.md
        sitebarname = "sitebar",
    },
    -- You can add multiple configurations
}
```

### About websites/sitebar.lua

Add some group areas for the website bar.

```lua
return {
    {
        -- Group name
        index = "Accomplished",
        -- Every group items
        {
            key = "Her Tears Were My Lights",
            value = "#",
        },
        {
            key = "The Sad Story of Emmeline Burns",
            value = "#"
        },
        {
            key = "Romance Detective",
            value = "#"
        }
    },
    -- You can add multiple sidebar areas
}
```

### About websites/list.lua

List all pages in the website.

```lua
return {
    {
        -- the markdown file is websites/${name}.md
        -- the html file is ${TargetDir}/${name}.html
        name = "index",
        
        title = "The Works Yvan Hom Translates",
        date = "2016-07-21",
        
        -- One of the above configurations
        config = "english"
    },
}
```

### About template

Concate template/header.txt, template/page.txt and template/footer.txt to the full template of the website page. It is in html-style, with some parameters surrounded with "{{" and "}}".

The template has same parameters:

* pagetitle: from page.title
* pagedate:  from page.date
* pagecontent: from the result of compiling websites/${page.name}.md
* siteowner: from config.siteowner
* sitetitle: from config.sitetitle
* siteyear:  from config.siteyear
* sitebar:   from template/sitebar.lua and websites/${config.sitebar}.lua

